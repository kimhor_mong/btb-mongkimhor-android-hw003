package com.example.homeworkthree.model;

import java.io.Serializable;

public class HumanBody implements Serializable {
    private int head;
    private int body;
    private int legs;

    public HumanBody(){}

    public HumanBody(int head, int body, int legs) {
        this.head = head;
        this.body = body;
        this.legs = legs;
    }

    public int getHead() {
        return head;
    }

    public void setHead(int head) {
        this.head = head;
    }

    public int getBody() {
        return body;
    }

    public void setBody(int body) {
        this.body = body;
    }

    public int getLegs() {
        return legs;
    }

    public void setLegs(int legs) {
        this.legs = legs;
    }

    @Override
    public String toString() {
        return "HumanBody{" +
                "head=" + head +
                ", body=" + body +
                ", legs=" + legs +
                '}';
    }
}
