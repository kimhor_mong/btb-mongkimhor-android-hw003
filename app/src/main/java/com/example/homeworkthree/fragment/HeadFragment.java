package com.example.homeworkthree.fragment;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.homeworkthree.R;
import com.example.homeworkthree.listner.SetHeadClickListener;

public class HeadFragment extends Fragment{
    private static SetHeadClickListener mHeadClickListener;
    private int mHeadResourceId;

    public HeadFragment(Context context, int headResourceId){
        this.mHeadResourceId = headResourceId;
        mHeadClickListener = (SetHeadClickListener) context;
    }

    public HeadFragment(int headResourceId){
        this.mHeadResourceId = headResourceId;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        return inflater.inflate(R.layout.fragment_head, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {

        AppCompatImageView imageViewHead = view.findViewById(R.id.image_view_head);
        imageViewHead.setImageResource(mHeadResourceId);

        if(mHeadClickListener != null){
            imageViewHead.setOnClickListener( v -> {
                mHeadClickListener.onHeadClicked(mHeadResourceId);
            });
        }

        super.onViewCreated(view, savedInstanceState);
    }
}