package com.example.homeworkthree.fragment;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.homeworkthree.R;
import com.example.homeworkthree.listner.SetBodyClickListener;

public class BodyFragment extends Fragment {
    private int mBodyResourceId;
    private SetBodyClickListener mBodyClickListener;

    public BodyFragment(int bodyResourceId){
     this.mBodyResourceId = bodyResourceId;
    }

    public BodyFragment(Context context, int bodyResourceId){
        this.mBodyResourceId = bodyResourceId;
        this.mBodyClickListener = (SetBodyClickListener) context;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_body, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {

        AppCompatImageView mImageViewBody = view.findViewById(R.id.image_view_body);
        mImageViewBody.setImageResource(mBodyResourceId);

        if(mBodyClickListener != null){
            mImageViewBody.setOnClickListener( v -> {
                mBodyClickListener.onBodyClicked(mBodyResourceId);
            });
        }

        super.onViewCreated(view, savedInstanceState);
    }
}