package com.example.homeworkthree.fragment;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.homeworkthree.R;
import com.example.homeworkthree.listner.SetLegsClickListener;

public class LegsFragment extends Fragment {
    private static SetLegsClickListener mLegsClickListener;
    private int mLegsResourceId;

    public LegsFragment(int legsResourceId) {
        this.mLegsResourceId = legsResourceId;
    }

    public LegsFragment(Context context, int legsResourceId) {
        this.mLegsResourceId = legsResourceId;
        mLegsClickListener = (SetLegsClickListener) context;

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_legs, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {

        AppCompatImageView mImageViewLegs = view.findViewById(R.id.image_view_legs);
        mImageViewLegs.setImageResource(mLegsResourceId);

        if(mLegsClickListener != null){
            mImageViewLegs.setOnClickListener(v ->{
                mLegsClickListener.onLegsClicked(mLegsResourceId);
            });
        }

        super.onViewCreated(view, savedInstanceState);
    }
}