package com.example.homeworkthree;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentTransaction;

import android.content.Intent;
import android.os.Bundle;

import com.example.homeworkthree.fragment.BodyFragment;
import com.example.homeworkthree.fragment.HeadFragment;
import com.example.homeworkthree.fragment.LegsFragment;
import com.example.homeworkthree.listner.SetBodyClickListener;
import com.example.homeworkthree.listner.SetHeadClickListener;
import com.example.homeworkthree.listner.SetLegsClickListener;
import com.example.homeworkthree.model.HumanBody;

public class HumanBodyActivity extends AppCompatActivity
        implements SetHeadClickListener, SetBodyClickListener, SetLegsClickListener {
    private HeadFragment mHeadFragment;
    private BodyFragment mBodyFragment;
    private LegsFragment mLegsFragment;

    private HumanBody mHumanBody;

    FragmentTransaction mFragmentTransaction;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_human_body);

        Intent intent = getIntent();
        mHumanBody = (HumanBody) intent.getSerializableExtra("human_body");

        showHumanBodyFragments();
    }

    private void showHumanBodyFragments(){
        mFragmentTransaction = getSupportFragmentManager().beginTransaction();

        mHeadFragment = new HeadFragment( this, mHumanBody.getHead());
        mFragmentTransaction.replace(R.id.fragment_container_view_head_detail, mHeadFragment);

        mBodyFragment = new BodyFragment(this, mHumanBody.getBody());
        mFragmentTransaction.replace(R.id.fragment_container_view_body_detail, mBodyFragment, null);

        mLegsFragment = new LegsFragment(this, mHumanBody.getLegs());
        mFragmentTransaction.replace(R.id.fragment_container_view_legs_detail, mLegsFragment, null);

        mFragmentTransaction.commit();
    }

    @Override
    public void onHeadClicked(int headResourceId) {
        int nextHeadResourceId = HumanBodyResources.getNextHeadAnatomy(headResourceId);

        mFragmentTransaction = getSupportFragmentManager().beginTransaction();

        mHeadFragment = new HeadFragment(this, nextHeadResourceId);
        mFragmentTransaction.replace(R.id.fragment_container_view_head_detail, mHeadFragment, null);
        mFragmentTransaction.commit();
    }

    @Override
    public void onBodyClicked(int bodyResourceId) {
        int nextBodyResourceId = HumanBodyResources.getNextBodyAnatomy(bodyResourceId);

        mFragmentTransaction = getSupportFragmentManager().beginTransaction();

        mBodyFragment = new BodyFragment(this, nextBodyResourceId);
        mFragmentTransaction.replace(R.id.fragment_container_view_body_detail, mBodyFragment, null);
        mFragmentTransaction.commit();
    }

    @Override
    public void onLegsClicked(int legsResourceId) {
        int nextLegsResourceId = HumanBodyResources.getNextLegsAnatomy(legsResourceId);

        mFragmentTransaction = getSupportFragmentManager().beginTransaction();

        mLegsFragment = new LegsFragment(this, nextLegsResourceId);
        mFragmentTransaction.replace(R.id.fragment_container_view_legs_detail, mLegsFragment, null);
        mFragmentTransaction.commit();
    }
}