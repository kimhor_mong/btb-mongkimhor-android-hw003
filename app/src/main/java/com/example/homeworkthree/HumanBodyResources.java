package com.example.homeworkthree;

import java.util.ArrayList;
import java.util.List;

public class HumanBodyResources {
    private static List<Integer> humanAnatomy;
    private static List<Integer> headAnatomy;
    private static List<Integer> bodyAnatomy;
    private static List<Integer> legsAnatomy;

    public static List<Integer> getHumanAnatomy(){
        initializeResources();

        return humanAnatomy;
    }

    public static int getNextHeadAnatomy(int resourceId){
        int currentHeadIndex = headAnatomy.indexOf(resourceId);
        if(currentHeadIndex == headAnatomy.size() - 1){
            currentHeadIndex = 0;
        } else {
            currentHeadIndex++;
        }
        return headAnatomy.get(currentHeadIndex);
    };

    public static int getNextBodyAnatomy(int resourceId){
        int currentHeadIndex = bodyAnatomy.indexOf(resourceId);
        if(currentHeadIndex == bodyAnatomy.size() - 1){
            currentHeadIndex = 0;
        } else {
            currentHeadIndex++;
        }
        return bodyAnatomy.get(currentHeadIndex);
    };

    public static int getNextLegsAnatomy(int resourceId){
        int currentHeadIndex = legsAnatomy.indexOf(resourceId);
        if(currentHeadIndex == legsAnatomy.size() - 1){
            currentHeadIndex = 0;
        } else {
            currentHeadIndex++;
        }
        return legsAnatomy.get(currentHeadIndex);
    };

    private static void initializeResources(){
        humanAnatomy = new ArrayList<>();
        humanAnatomy.add(R.drawable.head1);
        humanAnatomy.add(R.drawable.head2);
        humanAnatomy.add(R.drawable.head3);
        humanAnatomy.add(R.drawable.head4);
        humanAnatomy.add(R.drawable.head5);
        humanAnatomy.add(R.drawable.head6);
        humanAnatomy.add(R.drawable.head7);
        humanAnatomy.add(R.drawable.head8);
        humanAnatomy.add(R.drawable.head9);
        humanAnatomy.add(R.drawable.head10);
        humanAnatomy.add(R.drawable.head11);
        humanAnatomy.add(R.drawable.head12);

        humanAnatomy.add(R.drawable.body1);
        humanAnatomy.add(R.drawable.body2);
        humanAnatomy.add(R.drawable.body3);
        humanAnatomy.add(R.drawable.body4);
        humanAnatomy.add(R.drawable.body5);
        humanAnatomy.add(R.drawable.body6);
        humanAnatomy.add(R.drawable.body7);
        humanAnatomy.add(R.drawable.body8);
        humanAnatomy.add(R.drawable.body9);
        humanAnatomy.add(R.drawable.body10);
        humanAnatomy.add(R.drawable.body11);
        humanAnatomy.add(R.drawable.body12);

        humanAnatomy.add(R.drawable.legs1);
        humanAnatomy.add(R.drawable.legs2);
        humanAnatomy.add(R.drawable.legs3);
        humanAnatomy.add(R.drawable.legs4);
        humanAnatomy.add(R.drawable.legs5);
        humanAnatomy.add(R.drawable.legs6);
        humanAnatomy.add(R.drawable.legs7);
        humanAnatomy.add(R.drawable.legs8);
        humanAnatomy.add(R.drawable.legs9);
        humanAnatomy.add(R.drawable.legs10);
        humanAnatomy.add(R.drawable.legs11);
        humanAnatomy.add(R.drawable.legs12);

        headAnatomy = new ArrayList<>();
        headAnatomy = humanAnatomy.subList(0, 12);

        bodyAnatomy = new ArrayList<>();
        bodyAnatomy = humanAnatomy.subList(12, 24);

        legsAnatomy = new ArrayList<>();
        legsAnatomy = humanAnatomy.subList(24, 36);
    }
}
