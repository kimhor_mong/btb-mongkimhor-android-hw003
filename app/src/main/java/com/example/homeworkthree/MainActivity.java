package com.example.homeworkthree;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.widget.LinearLayout;

import com.example.homeworkthree.fragment.BodyFragment;
import com.example.homeworkthree.fragment.HeadFragment;
import com.example.homeworkthree.fragment.LegsFragment;
import com.example.homeworkthree.listner.SelectedAnatomyListener;
import com.example.homeworkthree.listner.SetBodyClickListener;
import com.example.homeworkthree.listner.SetHeadClickListener;
import com.example.homeworkthree.listner.SetLegsClickListener;
import com.example.homeworkthree.model.HumanBody;

import java.util.List;

public class MainActivity extends AppCompatActivity implements
        SelectedAnatomyListener, SetHeadClickListener, SetBodyClickListener, SetLegsClickListener {

    private List<Integer> mHumanAnatomy;
    private HumanBody mHumanBody;

    private HeadFragment mHeadFragment;
    private BodyFragment mBodyFragment;
    private LegsFragment mLegsFragment;

    private FragmentTransaction mFragmentTransaction;

    private boolean mIsTwoPane = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // initialize values for recycler view
        initializeRecyclerView();

        // set default human body
        mHumanBody = getDefaultHumanBody();

        // check device's screen - if tablet, then two panes
        if (isTwoPanesLayout()) {
            mIsTwoPane = true;
            showHumanBodyFragments();
        } else {
            // handle click when the screen is mobile
            onButtonNextClick();
        }
    }

    private void initializeRecyclerView() {
        // get array of human anatomy
        mHumanAnatomy = HumanBodyResources.getHumanAnatomy();

        // initialize recycler view
        RecyclerView mRecyclerView = findViewById(R.id.recycler_view_human_anatomy);

        // set layout manager for recycler view
        GridLayoutManager gridLayoutManager = new GridLayoutManager(getApplicationContext(), 2);
        gridLayoutManager.setOrientation(GridLayoutManager.VERTICAL);
        mRecyclerView.setLayoutManager(gridLayoutManager);

        // set adapter for recycler view
        HumanBodyAdapter humanBodyAdapter = new HumanBodyAdapter(this, mHumanAnatomy);
        mRecyclerView.setAdapter(humanBodyAdapter);
    }

    private void showHumanBodyFragments() {
        mFragmentTransaction = getSupportFragmentManager().beginTransaction();

        if (mIsTwoPane) {
            mHeadFragment = new HeadFragment(this, mHumanBody.getHead());
            mBodyFragment = new BodyFragment(this, mHumanBody.getBody());
            mLegsFragment = new LegsFragment(this, mHumanBody.getLegs());
        } else {
            mHeadFragment = new HeadFragment(mHumanBody.getHead());
            mBodyFragment = new BodyFragment(mHumanBody.getBody());
            mLegsFragment = new LegsFragment(mHumanBody.getLegs());
        }
        mFragmentTransaction.replace(R.id.fragment_container_view_head, mHeadFragment, null);
        mFragmentTransaction.replace(R.id.fragment_container_view_body, mBodyFragment, null);
        mFragmentTransaction.replace(R.id.fragment_container_view_legs, mLegsFragment, null);

        mFragmentTransaction.commit();
    }

    private void onButtonNextClick() {
        AppCompatButton mButtonNext = findViewById(R.id.btn_next);
        mButtonNext.setOnClickListener(view -> {
            Intent intent = new Intent(MainActivity.this, HumanBodyActivity.class);
            intent.putExtra("human_body", mHumanBody);
            startActivity(intent);
        });
    }

    @Override
    public void onSelectedAnatomy(int position) {
        changeHumanAnatomy(position);
    }
    @Override
    public void onHeadClicked(int headResourceId) {
        if (!mIsTwoPane) return;

        int nextHeadResourceId = HumanBodyResources.getNextHeadAnatomy(headResourceId);

        mFragmentTransaction = getSupportFragmentManager().beginTransaction();
        mHeadFragment = new HeadFragment(this, nextHeadResourceId);
        mFragmentTransaction.replace(R.id.fragment_container_view_head, mHeadFragment, null);
        mFragmentTransaction.commit();
    }

    @Override
    public void onBodyClicked(int bodyResourceId) {
        if (!mIsTwoPane) return;

        int nextBodyResourceId = HumanBodyResources.getNextBodyAnatomy(bodyResourceId);

        mFragmentTransaction = getSupportFragmentManager().beginTransaction();
        mBodyFragment = new BodyFragment(this, nextBodyResourceId);
        mFragmentTransaction.replace(R.id.fragment_container_view_body, mBodyFragment, null);
        mFragmentTransaction.commit();
    }

    @Override
    public void onLegsClicked(int legsResourceId) {
        if (!mIsTwoPane) return;

        int nextLegsResourceId = HumanBodyResources.getNextLegsAnatomy(legsResourceId);

        mFragmentTransaction = getSupportFragmentManager().beginTransaction();
        mLegsFragment = new LegsFragment(this, nextLegsResourceId);
        mFragmentTransaction.replace(R.id.fragment_container_view_legs, mLegsFragment, null);
        mFragmentTransaction.commit();
    }

    private void changeHumanAnatomy(int position) {
        String resourceName = this.getResources().getResourceEntryName(mHumanAnatomy.get(position));
        int resourceValue = mHumanAnatomy.get(position);

        if (resourceName.contains("head")) {
            mHumanBody.setHead(resourceValue);
        } else if (resourceName.contains("body")) {
            mHumanBody.setBody(resourceValue);
        } else {
            mHumanBody.setLegs(resourceValue);
        }

        if(mIsTwoPane) showHumanBodyFragments();
    }

    private HumanBody getDefaultHumanBody() {
        int defaultHead = R.drawable.head1;
        int defaultBody = R.drawable.body1;
        int defaultLegs = R.drawable.legs1;
        return new HumanBody(defaultHead, defaultBody, defaultLegs);
    }

    private boolean isTwoPanesLayout() {
        LinearLayout linearLayoutHumanBody = findViewById(R.id.linear_layout_human_body);
        return linearLayoutHumanBody != null;
    }
}