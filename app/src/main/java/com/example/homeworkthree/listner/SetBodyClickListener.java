package com.example.homeworkthree.listner;

public interface SetBodyClickListener {
    public void onBodyClicked(int bodyResourceId);
}
