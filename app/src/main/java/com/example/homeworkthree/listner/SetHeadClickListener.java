package com.example.homeworkthree.listner;

public interface SetHeadClickListener {
    public void onHeadClicked(int headResourceId);
}
