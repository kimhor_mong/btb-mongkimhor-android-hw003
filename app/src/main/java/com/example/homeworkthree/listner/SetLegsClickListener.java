package com.example.homeworkthree.listner;

public interface SetLegsClickListener {
    public void onLegsClicked(int legsResourceId);
}
