package com.example.homeworkthree;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.example.homeworkthree.listner.SelectedAnatomyListener;

import java.util.List;

public class HumanBodyAdapter extends RecyclerView.Adapter<HumanBodyAdapter.HumanBodyViewHolder> {

    private List<Integer> mHumanAnatomies;
    private Context mContext;
    private SelectedAnatomyListener mSelectedAnatomyListener;

    public HumanBodyAdapter(Context context, List<Integer> humanAnatomies) {
        this.mContext = context;
        this.mHumanAnatomies = humanAnatomies;
        mSelectedAnatomyListener = (SelectedAnatomyListener) context;
    }

    @NonNull
    @Override
    public HumanBodyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.human_anatomy_item, parent, false);
        return new HumanBodyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull HumanBodyViewHolder holder, int position) {
        holder.humanAnatomy.setImageResource(mHumanAnatomies.get(position));
        holder.humanAnatomyWrapper.setOnClickListener(view -> {
            mSelectedAnatomyListener.onSelectedAnatomy(position);
        });
    }

    @Override
    public int getItemCount() {
        return mHumanAnatomies.size();
    }

    public class HumanBodyViewHolder extends RecyclerView.ViewHolder {
        AppCompatImageView humanAnatomy;
        CardView humanAnatomyWrapper;

        public HumanBodyViewHolder(@NonNull View itemView) {
            super(itemView);
            humanAnatomy = itemView.findViewById(R.id.image_view_human_anatomy);
            humanAnatomyWrapper = itemView.findViewById(R.id.card_view_human_anatomy_wrapper);
        }
    }
}
